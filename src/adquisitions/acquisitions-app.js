import acquisitionsStore from "./store/acquisitions-store";
import {ViewTable} from "./presentation/table-products/table-products";
import { ViewButtons } from "./presentation/buttons/buttons";
import { ViewModal } from "./presentation/modal/modal";
import { saveProduct } from "./uses/save-product";

/**
 *
 * @param {HTMLDivElement} element
 */

export const AcquisitionsApp = async (element) => {
  element.innerHTML = "Cargando . . . ";
  await acquisitionsStore.loadNextPage();

  //console.log(acquisitionsStore.getAcquisitions())
element.innerHTML = " ";
ViewTable(element);

ViewButtons(element);

ViewModal(element, async(productLike) =>{
  const product = await saveProduct(productLike);
  //console.log(product);
  acquisitionsStore.acquiOnChange(product);
  ViewTable();
  
});


};
