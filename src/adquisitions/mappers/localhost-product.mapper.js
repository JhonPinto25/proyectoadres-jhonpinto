import { Product } from "../models/product";

/**
 * 
 * @param {Like<Product>} localhostDataProduct 
 * @returns {Product}
 */
export const localhostProductToModel = (localhostDataProduct) => {
  
  const { 
    id, 
    nombre_producto, 
    unidad, 
    proveedor, 
    cantidad,
    valor, 
    valorTotal,
    presupuesto,
    fecha,
    documentacion,
    isActive

    
  } = localhostDataProduct;

  return new Product({
    id,
    nombreProducto: nombre_producto,
    unidad,
    proveedor,
    cantidad,
    valor,
    valorTotal,
    presupuesto,
    fecha,
    documentacion,
    isActive
    
  });
};
