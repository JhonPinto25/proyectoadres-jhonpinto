


/**
 * @param {Product} product
 */

export const productModelToLocalhost = (product)=>{

  const { 
    id, 
    nombreProducto, 
    unidad, 
    proveedor, 
    cantidad,
    valor, 
    valorTotal,
    presupuesto,
    fecha,
    documentacion,
    isActive
    
  } = product;

  return {
    id, 
    nombre_producto: nombreProducto, 
    unidad, 
    proveedor, 
    cantidad,
    valor, 
    valorTotal,
    presupuesto,
    fecha,
    documentacion,
    isActive
    
  }
  }

