export class Audit{

  /**
   * 
   * @param {Like<Historic>} historicDataModel 
   */
  constructor({id , newVersion, oldVersion }){
    this.id = id,
    this.newVersion = newVersion,
    this.oldVersion = oldVersion,
    this.fecha = new Date()
  }

}