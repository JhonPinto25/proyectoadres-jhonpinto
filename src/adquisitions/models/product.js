export class Product{

  /**
   * 
   * @param {Like<Acquisition>} acquisitionDataModel 
   */
  constructor({id , nombreProducto, unidad, proveedor, cantidad, valor, valorTotal, presupuesto, fecha, documentacion}){

    this.id = id,
    this.nombreProducto = nombreProducto,
    this.unidad= unidad,
    this.proveedor= proveedor,
    this.cantidad = cantidad
    this.valor = valor,
    this.valorTotal = valorTotal,
    this.presupuesto = presupuesto,
    this.fecha = fecha,
    this.documentacion = documentacion,
    this.isActive = true
    

  }

}