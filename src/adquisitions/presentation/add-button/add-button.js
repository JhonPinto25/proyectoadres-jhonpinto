
import "./add-button.css"

import {showModal} from '../modal/modal'

/**
 * 
 * @param {HTMLDivElement} element 
 */




export const ViewAddButton = (element) =>{

  const fabButton = document.createElement('button');
  fabButton.innerText ="Agregar Adquisición +"; 
  fabButton.classList.add('fab-button');

  element.append(fabButton);
  fabButton.addEventListener('click', ()=>{
    //throw new Error("no implementadooo");
    showModal();  
  })

}
