import "./buttons.css";
import acquisitionsStore from "../../store/acquisitions-store";
import { ViewTable } from "../table-products/table-products";

/**
 *
 * @param {HTMLDivElement} element
 */

export const ViewButtons = (element) => {
  const nextButton = document.createElement("button");
  nextButton.innerText = "Página Siguiente >";
  const previousButton = document.createElement("button");
  previousButton.innerText = "< Página Anterior";

  const currentPageLabel = document.createElement("span");
  currentPageLabel.id = "current-page";
  currentPageLabel.innerText = acquisitionsStore.getcurrentPage();

  element.append(previousButton, currentPageLabel, nextButton);

  //Se implementa la funcion para dar en siguiente

  nextButton.addEventListener("click", async () => {
    await acquisitionsStore.loadNextPage();
    currentPageLabel.innerText = acquisitionsStore.getcurrentPage();

    ViewTable(element);
  });

  //Se implementa la funcion para anterior

  previousButton.addEventListener("click", async () => {
    await acquisitionsStore.loadPreviousPage();
    currentPageLabel.innerText = acquisitionsStore.getcurrentPage();
    ViewTable(element);
  });
};
