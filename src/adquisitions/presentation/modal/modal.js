import { getProductById } from "../../uses/get-product-by-id";
import "./modal.css";
import htmlModal from "./modal.html?raw";

/**
 *
 * @param {HTMLDivElement} element
 * @param {(productLike) => Promise<void>} callbackSaveUpdateProduct
 */

let modal;
let form;
let loadedProduct = {};

export const ViewModal = (element, callbackSaveUpdateProduct) => {
  if (modal) {
    return;
  } else {
    modal = document.createElement("div");
    modal.innerHTML = htmlModal;
    modal.className = "modal-container hide-modal";

    form = modal.querySelector("form");

    modal.addEventListener("click", (event) => {
      //console.log(event.target.className);

      if (event.target.className === "modal-container") {
        hideModal();
      }
    });

    form.addEventListener("submit", async (event) => {
      event.preventDefault();
      //console.log("Formulario enviado.")
      const formData = new FormData(form);
      const productLike = { ...loadedProduct };

      for (const [key, value] of formData) {
        if (key === "valor") {
          productLike[key] = Number(value);
          continue;
        }

        if (key === "cantidad") {
          productLike[key] = Number(value);
          continue;
        }

        if (key === "isActive") {
          productLike[key] = value === "on" ? true : false;
          continue;
        }

        productLike[key] = value;
      }

      //console.log(productLike);
      //guardar usuario
      await callbackSaveUpdateProduct(productLike);
    });
    element.append(modal);
  }
};

export const showModal = async (id) => {
  modal?.classList.remove("hide-modal");
  loadedProduct = {};
  if (!id) {
    const nameSaveButton = document.querySelector(".submit-button");
    //console.log(nameSaveButton);
    nameSaveButton.innerHTML = "Guardar";

    return;
  }

  const nameEditButton = document.querySelector(".submit-button");
 // console.log(nameEditButton);
  nameEditButton.innerHTML = "Actualizar";

  const product = await getProductById(id);
  setFormValues(product);
};

export const hideModal = () => {
  modal?.classList.add("hide-modal");
  form?.reset();
};

const setFormValues = (product) => {
  form.querySelector('[name="nombreProducto"]').value = product.nombreProducto;
  form.querySelector('[name="unidad"]').value = product.unidad;
  form.querySelector('[name="proveedor"]').value = product.proveedor;
  form.querySelector('[name="cantidad"]').value = product.cantidad;
  form.querySelector('[name="valor"]').value = product.valor;
  form.querySelector('[name="valorTotal"]').value = product.valorTotal;
  form.querySelector('[name="presupuesto"]').value = product.presupuesto;
  form.querySelector('[name="fecha"]').value = product.fecha;
  form.querySelector('[name="documentacion"]').value = product.documentacion;

  loadedProduct = product;
};
