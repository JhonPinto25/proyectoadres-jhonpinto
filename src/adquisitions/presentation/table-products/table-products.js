import acquisitionsStore from "../../store/acquisitions-store";
import { deleteProductById } from "../../uses/delete-product-by-id";
import { hideModal, showModal } from "../modal/modal";
import "./table-products.css";

let table;

const createTable = () => {
  const table = document.createElement("table");
  const Headerstable = document.createElement("thead");
  const bodyTable = document.createElement("tbody");

  Headerstable.innerHTML = `
  <tr>
  <th>Id</th>
  <th>Tipo de bien o servicio</th>
  <th>Unidad</th>
  <th>Proveedor</th>
  <th>Cantidad</th>
  <th>Valor Unitario</th>
  <th>Valor Total</th>
  <th>Presupuesto</th>
  <th>Fecha Adquisición</th>
  <th>Documentación</th>
  <th>Acciones</th>
  
  
  </tr>
  `;

  table.append(Headerstable, bodyTable);
  return table;
};

const tableSelectListener = (event) => {
  /**
   * @param {String|Number} id
   */

  //console.log(event.target);
  const element = event.target.closest(".select-product");
  
  

  if (!element) {
    return;
  }

  const id = element.getAttribute("data-id");
 //console.log(id);

  

  showModal(id);
};

// listener para eliminar regstros de la tabla

const tableDeleteListener = async (event) => {
  /**
   * @param {String|Number} id
   */

  const element = event.target.closest(".delect-product");

  if (!element) {
    return;
  }

  const id = element.getAttribute("data-id");

  try {
    await deleteProductById(id);
    await acquisitionsStore.reloadPage();
    document.querySelector("#current-page").innerText =
      acquisitionsStore.getcurrentPage();
    ViewTable();
  } catch (error) {
    console.log(error);
    alert("No se puedo eliminar este registro");
  }
};

export const ViewTable = (element) => {
  const products = acquisitionsStore.getAcquisitions();

  if (!table) {
    table = createTable();
    element.append(table);

    // Se crean los listener de la tabla
    table.addEventListener("click", (event) => {
      tableSelectListener(event);
    });
    table.addEventListener("click", (event) => {
      tableDeleteListener(event);
    });
  }

  let tableHTML = "";

  products.forEach((product) => {
    tableHTML =
      tableHTML +
      `

  <tr>
  <td>${product.id}</td>
  <td>${product.nombreProducto}</td>
  <td>${product.unidad}</td>
  <td>${product.proveedor}</td>
  <td>${product.cantidad}</td>
  <td>${Intl.NumberFormat('es-MX').format(product.valor)}</td>
  <td>${ Intl.NumberFormat('es-MX').format(product.valor * product.cantidad) }</td>
  <td>${Intl.NumberFormat('es-MX').format(product.presupuesto)}</td>
  <td>${product.fecha}</td>
  <td>${product.documentacion}</td>
  
  <td>
  <img src="Images/edit.png" alt="edit" class="select-product" data-id="${product.id}" title="Editar" />   <img src="Images/delete.png" alt="delete" class="delect-product" data-id="${product.id}" title="Eliminar" />   
  </td>
  </tr>
  `;
  });

  table.querySelector("tbody").innerHTML = tableHTML;

  hideModal();
};
