import { loadAcquisitionsByPage } from "../uses/load-acquisitions-by-page";

const state = {
  currentPage: 0,
  acquisitions: [],
};

const loadNextPage = async () => {
  const products = await loadAcquisitionsByPage(state.currentPage + 1);
  if (products.length === 0) {
    return;
  }

  state.currentPage = state.currentPage + 1;
  state.acquisitions = products;
};

const loadPreviousPage = async () => {
  if (state.currentPage === 1) {
    console.log("Estoy en la pag 1");
    return;
  }
    const products = await loadAcquisitionsByPage(state.currentPage - 1);
    //console.log(state.currentPage-1);
    state.currentPage = state.currentPage - 1;
    state.acquisitions = products;
  
};

const acquiOnChange = (updatedProduct) => {

  let wasFound= false;

  state.acquisitions = state.acquisitions.map(product =>{
    if(product.id === updatedProduct.id){
      wasFound = true;
      return updatedProduct;

    }
    return product;
  });

  if (state.acquisitions.length<10 && !wasFound){ 
    state.acquisitions.push(updatedProduct)
    
  }
 
};

const reloadPage = async() => {
  const products = await loadAcquisitionsByPage(state.currentPage);
  if (products.length === 0) {
    await loadPreviousPage();
    return;
  }

  state.acquisitions = products;
};

export default {
  loadNextPage,
  loadPreviousPage,
  acquiOnChange,
  reloadPage,

  /**
   *
   * @returns { Product[]}
   */

  getAcquisitions: () => [...state.acquisitions],

  /**
   *
   * @returns { Number }
   */
  getcurrentPage: () => state.currentPage,
};
