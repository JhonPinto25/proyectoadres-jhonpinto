import { updateProduct } from "./save-product";
import { localhostProductToModel } from "../mappers/localhost-product.mapper";
import { productModelToLocalhost } from "../mappers/product-to-localhost.mapper";

export const deleteProductById = async (id) => {
  /*   const url = `${import.meta.env.VITE_BASE_URL}/acquisitions/${id}`;
  const res = await fetch(url, {
    method: "DELETE",
    
   
  }); 

  const deleteResult = await res.json();*/

  const url = `${import.meta.env.VITE_BASE_URL}/acquisitions/${id}`;
  const res = await fetch(url);
  const data = await res.json();

  const product = localhostProductToModel(data);
  product.isActive = false;
  const productToSave = productModelToLocalhost(product);

  updateProduct(productToSave);

  return product;
};
