import { localhostProductToModel } from "../mappers/localhost-product.mapper";
import { Product } from "../models/product";

/**
 *
 * @param {String|Number} id
 * @returns { Promise<Product>}
 
 */

export const getProductById = async (id) => {
  //se crea la constante url donde nos treamos la data
  const url = `${import.meta.env.VITE_BASE_URL}/acquisitions/${ id }`;
  const res = await fetch(url);
  const data = await res.json();



  const product = localhostProductToModel(data);
  //console.log({product})

  return product;
};
