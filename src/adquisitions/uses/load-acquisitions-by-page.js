import { localhostProductToModel } from '../mappers/localhost-product.mapper';

/**
 *
 * @param {Number} page
 * @returns { Promise<Product[]>}
 
 */


export const loadAcquisitionsByPage = async (page = 1) => {
  //se crea la constante url donde nos traemos la data
  const url = `${ import.meta.env.VITE_BASE_URL}/acquisitions?isActive=true&_page=${ page }`;
  const res = await fetch(url);
  const data = await res.json();
  //console.log(data);
 

  const products = data.map( productLike => localhostProductToModel (productLike));
  //const products = data.map( localhostProductToModel);


  //console.log(products);

  return products;

};
