import { Audit } from "../models/audit";


export const createHistoricProduct = async ( newProduct, oldProduct) => {
  const url = `${import.meta.env.VITE_BASE_URL}/historic`;
  //console.log("insideHistoric", oldProduct);
  const audit= {
    oldVersion: oldProduct, 
    newVersion: newProduct
  };
  
  const auditToSave = new Audit(audit);
  
  const res = await fetch(url, {
    method: "POST",
    body: JSON.stringify(auditToSave),
    headers: {
      "Content-Type": "application/json",
    },
  });

  const newAudit = await res.json();
  console.log({newAudit});

  return newAudit;
 
};

