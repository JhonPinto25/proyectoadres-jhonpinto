import { localhostProductToModel } from "../mappers/localhost-product.mapper";
import { productModelToLocalhost } from "../mappers/product-to-localhost.mapper";
import { Product } from "../models/product";
import { hideModal } from "../presentation/modal/modal";
import { getProductById } from "./get-product-by-id";
import { createHistoricProduct } from "./save-historic";

/**
 *
 * @param {Like<Product>} productLike
 */

export const saveProduct = async (productLike) => {
  const product = new Product(productLike);

  let oldProduct = await getOldProduct(productLike);
  if (
    !product.nombreProducto ||
    !product.unidad ||
    !product.proveedor ||
    !product.cantidad ||
    !product.valor ||
    !product.presupuesto ||
    !product.fecha ||
    !product.documentacion
  ) {
  alert (
      "Por favor, digitar el nombre, la unidad, el proveedor, la cantidad, el valor, el presupuesto, la fecha y la documentación. Gracias."
    );

    
  }

  const productToSave = productModelToLocalhost(product);
  let productUpdated;

  if (product.id) {
    productUpdated = await updateProduct(productToSave);
  } else {
    productUpdated = await createProduct(productToSave);
  }

  const productMapped = localhostProductToModel(productUpdated);

  createHistoricProduct(productMapped, oldProduct);
  return productMapped;
};

const getOldProduct = async (product) => {
  let oldProduct;
  if (product.id) {
    oldProduct = await getProductById(product.id);
    //console.log("juan" , oldProduct);
  }
  return oldProduct;
};

const createProduct = async (product) => {
  const url = `${import.meta.env.VITE_BASE_URL}/acquisitions`;
  const res = await fetch(url, {
    method: "POST",
    body: JSON.stringify(product),
    headers: {
      "Content-Type": "application/json",
    },
  });

  const newProduct = await res.json();
  console.log({ newProduct });
  return newProduct;
};

export const updateProduct = async (product) => {
  const url = `${import.meta.env.VITE_BASE_URL}/acquisitions/${product.id}`;
  const res = await fetch(url, {
    method: "PATCH",
    body: JSON.stringify(product),
    headers: {
      "Content-Type": "application/json",
    },
  });

  const updatedProduct = await res.json();
  //console.log({ updatedProduct });
  return updatedProduct;
};
